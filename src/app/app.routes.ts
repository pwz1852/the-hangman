import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    title: 'Welcome to the Hangman Game!',
    loadChildren: () => import('./welcome/welcome.module').then(m => m.WelcomeModule)
  },
  {
    path: 'game',
    title: 'Hangman Game',
    loadChildren: () => import('./game/game.module').then(m => m.GameModule),
  }
];
