import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, provideRouter } from '@angular/router';
import { GameComponent } from './components/game/game.component';
import { MockApiService } from './services/mock-api.service';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  {
    path: '',
    component: GameComponent,
  },
];


@NgModule({
  declarations: [
    GameComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  providers: [
    provideRouter(routes),
    MockApiService,
  ]
})
export class GameModule { }
