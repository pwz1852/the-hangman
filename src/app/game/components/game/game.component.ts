import { Component, HostListener } from '@angular/core';
import { MockApiService } from '../../services/mock-api.service';
import type { Answer } from '../../../types/Answear';
import { differenceInMinutes } from 'date-fns';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrl: './game.component.scss'
})
export class GameComponent {
  letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
  hitLetters: string[] = [];
  missLetters: string[] = [];

  startGameTime = new Date();
  endGameTime?: Date;
  
  answers: Answer[] = [];
  actualAnswerIndex = 0;
  showModal = false;

  constructor(private mockApiService: MockApiService) {}

  ngOnInit() {
    this.setRandomAnswers();
  }

  setRandomAnswers() {
    this.mockApiService.getAnswers().subscribe((answers) => {
      this.answers = getRandomArrayElements(answers, 5);
    });
  }

  @HostListener('window:keyup', ['$event'])
  onKeyup(event: KeyboardEvent) {
    this.hitLetter(event.key);
  }

  newGame() {
    this.hitLetters = [];
    this.missLetters = [];
    this.actualAnswerIndex = 0;
    this.setRandomAnswers();
    this.startGameTime = new Date();
    this.endGameTime = undefined;
    this.showModal = false;
  }

  hitLetter(letter: string) {
    const word = this.answers[this.actualAnswerIndex].word;
    const hasLetter = word.toLocaleLowerCase().includes(letter.toLocaleLowerCase());

    if (this.hitLetters.includes(letter.toLocaleLowerCase())
      || this.missLetters.includes(letter.toLocaleLowerCase())
      || this.showModal) {
      return;
    }

    if (hasLetter) {
      this.hitLetters.push(letter);
    } else {
      this.missLetters.push(letter);
    }

    if (this.missLetters.length > 5 || this.actualAnswerIndex === this.answers.length - 1) {
      this.endGameTime = new Date();
      this.showModal = true;
    }

    if (this.hitLetters.length === new Set(word.split('')).size) {
      this.nextGame();
    }
  }

  nextGame() {
    this.hitLetters = [];
    this.missLetters = [];
    this.actualAnswerIndex++;
  };

  get gameTimeMin() {
    return differenceInMinutes(this.startGameTime, this.endGameTime || new Date());
  }
}

function getRandomArrayElements<T>(arr: T[], numberOfElems: number): T[] {
  if (arr.length <= numberOfElems) {
    return arr;
  }

  const newArr: T[] = [];
  const indices: number[] = [];

  while (newArr.length < numberOfElems) {
    const randomIndex = Math.floor(Math.random() * arr.length);

    if (indices.includes(randomIndex) === false) {
      newArr.push(arr[randomIndex]);
      indices.push(randomIndex);
    }
  }

  return newArr
}