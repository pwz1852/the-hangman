import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, tap } from 'rxjs';
import type { Answer } from '../../types/Answear';

@Injectable()
export class MockApiService {
  answers: Answer[] = [];

  constructor(private http: HttpClient) { }

  getAnswers = (): Observable<Answer[]> => {
    if (this.answers.length > 0) {
      return of(this.answers);
    }

    return this.http.get<Answer[]>('/assets/mocks/answers.json').pipe(tap((answers) => {
      this.answers = answers;
    }));
  }
}
