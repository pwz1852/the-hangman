import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { Routes, provideRouter } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    title: 'Welcome to the Hangman Game!',
    component: WelcomeComponent,
  }
]

@NgModule({
  declarations: [
    WelcomeComponent
  ],
  imports: [
    CommonModule,
  ],
  providers: [provideRouter(routes)]
})
export class WelcomeModule { }
